#PROGRAM
Light Moment
#AUTHOR
Noah Paola Andreina Vargas Ocando
#TIME
May 2019
#SPACE
Montreal, Canada

# # # # # # # # # # # # # # # # # # 
# INSTALLATION
# # # # # # # # # # # # # # # # # # 
 - Open a windows command line
 - Install dependencies : 
 > * py -m pip install --upgrade pip
 > * py -m pip install --upgrade numpy
 > * py -m pip install --upgrade matplotlib
 > * py -m pip install --upgrad opencv-python
 > * py -m pip install --upgrade pip wheel setuptools
 > * py -m pip install --upgrade pip docutils pygments kivy.deps.sdl2 kivy.deps.glew
 > * py -m pip install --upgrade kivy.deps.gstreamer
 > * py -m pip install --upgrade kivy.deps.angle
 > * py -m pip install --upgrade kivy
 > * py -m pip install --upgrade pylint
 - Import git :
 >  * git clone https://Ztar_473@bitbucket.org/Ztar_473/synthese-lightmoment.git
 - Run program
 * Open path to main.py in file explorer
 * Open command line at path
 > * Python main.py