import numpy as np
import cv2

class Hand():
    #si c'est plus petit que
    min_negligeable_area = 300
    max_negligeable_area = 250000
    # negligeable_area = 5000
    #ce n'est pas une main
    def __init__(self, x, y , width , height, hull, contour , area):
        self.x = x
        self.y = y
        self.center = ( int ( x + (width/2) ) , int( y + (height/2) ) )
        self.width = width
        self.height = height
        self.area = self.getArea()
        self.hull = hull
        self.contour = cv2.UMat(np.array(contour, dtype=np.int32))
        self.fingertips = self.calcFingertips()
        self.shape = None
        self.valid = self.isValid()
    
    def getArea(self):
        return self.width * self.height

    def isValid(self):
        if self.area < Hand.min_negligeable_area or self.area > Hand.max_negligeable_area:
            return False
        return True

    def calcFingertips(self):
        # print("---START CONTOUR---")
        # print(type(self.contour))
        # # print(self.contour)
        # print("---END CONTOUR---")
        # print("---START HULL---")
        # print(type(self.hull))
        # # print(self.hull)
        # print("---END HULL---")
        # # test = cv2.UMat(np.array(self.contour, dtype=np.uint8)) #uint8
        # defects = cv2.convexityDefects(self.contour, self.hull)
        # # print(defects)
        pass

    def calcShape(self):
        pass