

from classes.Utils import Utils
from classes.HandRelationshipEval import HandRelationshipEval
from classes.Relationship import Relationship

class HandActionEval():
    angle_error_buffer=10
    pixel_error_buffer=50
    def __init__(self, hands):
        self.name = ""
        self.handList = []
        self.relationships = []
        self.actions = []

        for each in hands:
            self.handList.append(each)

        self.evaluateHands()
        self.evaluateRelationships()

        self.valid = self.isValid()
    def evaluateHands(self):
        for index in range(len(self.handList)):
            if index > 0:
                handTo = self.handList[index]
                handFrom = self.handList[index-1]
                distance = Utils.getDist(handFrom.center, handTo.center)
                pivotPoint = (handFrom.center[0],0)
                movementAngle = Utils.getAngle(handFrom.center,handTo.center,pivotPoint)
                sizeTranslation = Utils.getSizeTranslation(handFrom.area , handTo.area)
                relationship = HandRelationshipEval(handFrom,handTo,distance,movementAngle, sizeTranslation)
                if relationship.valid == True:
                    self.relationships.append(relationship)
        
        pass


    def isValid(self):
        if len(self.actions) > 0 or len(self.relationships) > 1:
            self.actions = self.actions[0]
            return True
        return False

    def evaluateRelationships(self):
        for each in self.relationships:
            self.actions.append(each.name)