# Kivy graphics
from kivy.graphics.instructions import Instruction
from kivy.graphics.instructions import InstructionGroup

# Kivy Widgets
from kivy.uix.image import Image

# Light Moment Libraries
from classes.Particle import Particle
from classes.VideoProxy import VideoProxy

class GameFactory:
    modes = ["Seulement des particules" , "Seulement les ombres" , "Ombres et particules"]
    
    def __init__(self, parent):
        self.parent = parent

    def startGame(self, nbParticles, field, modeIndex, theme):
        field.voidField()
        for i in range(nbParticles):
            self.parent.particleList.append(Particle(self.parent, theme))
        self.parent.inGame = True
        self.initVideo()

    def startMode(self):
        pass
    
    def initVideo(self):
        # self.parent.view.startVideo()
        self.parent.vidProxy = VideoProxy(self.parent)
        self.parent.view.videoImage=Image()
        self.parent.view.backgroundLayout.add_widget(self.parent.view.videoImage)