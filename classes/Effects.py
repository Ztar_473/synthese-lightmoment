
class Effects():
    possible_effects = [
        {
            "name":"right",
            "actions":['right'],
            "acceleration":(1,0)
        },
        {
            "name":"left",
            "actions":['left'],
            "acceleration":(-1,0)
        },
        {
            "name":"up",
            "actions":['up'],
            "acceleration":(0,1)
        },
        {
            "name":"down",
            "actions":['down'],
            "acceleration":(0,-1)
        },
        {
            "name":"calm",
            "actions":['up', 'down'],
            "acceleration":(-2,-2)
        },
        {
            "name":"calm",
            "actions":['down', 'up'],
            "acceleration":(-2,-2)
        },
    ]