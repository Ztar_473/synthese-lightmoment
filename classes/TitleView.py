# widgets
from kivy.uix.label import Label
from kivy.uix.button import  Button
from kivy.uix.spinner import Spinner
from kivy.uix.slider import Slider

class TitleView:
    texts = {
            "titleText" : "Light Moment",
            "sliderText" : "Nombre de particules",
            "spinner1Text" : "Theme",
            "spinner2Text" : "Mode",
        }
    def __init__(self, parent, layout, theme):
        self.parent = parent
        self.layout = layout
        self.theme = theme
        # Title widgets
        self.titleLabel = None
        self.btnStart = None
        self.btnParams = None
        self.labelSliderParticles = None
        self.sliderParticles = None
        self.labelNbParticles = None
        self.labelSpinnerTheme = None
        self.spinnerTheme = None
        self.labelSpinnerMode = None
        self.spinnerMode = None
        pass
    
    def setTitleWidgets(self, layout):
        self.titleLabel = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["titleText"]+"[/color]",
                        # Size and position
                        size_hint=(.5, .20),pos_hint={'x':0, 'y':.8},
                        # Parameters by theme
                        font_size=self.theme.getParam("title_size"), markup=True)
        self.btnStart = Button(   # Text in the button
                        text='Commencer',
                        # Size and position
                        size_hint=(.15, .1),pos_hint={'x':.80, 'y':.05})
        self.btnParams = Button(   # Text in the button
                        text='Parametres',
                        # Size and position
                        size_hint=(.11, .06),pos_hint={'x':.05, 'y':.05})
        

        self.labelSliderParticles = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["sliderText"]+"[/color]",
                        # Size and position
                        size_hint=(.28, .05),pos_hint={'x':.05, 'y':.75},
                        markup=True)
        self.sliderParticles = Slider(value_track=True,
                        max=self.parent.parent.maxNbParticles, min=self.parent.parent.minNbParticles, step=1,
                        # Size and position
                        size_hint=(.27, .05),pos_hint={'x':.04, 'y':.72},
                        cursor_size=(16,16),
                        # Parameters by theme
                        value_track_color=self.theme.getParam("main_color_dec"))
        self.labelNbParticles = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+str(self.sliderParticles.value)+"[/color]",
                        # Size and position
                        size_hint=(0, .05),pos_hint={'x':.32, 'y':.72},
                        markup=True)


        self.labelSpinnerTheme = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["spinner1Text"]+"[/color]",
                        # Size and position
                        size_hint=(.28, .05),pos_hint={'x':.05, 'y':.65},
                        markup=True)
        self.spinnerTheme = Spinner(  # Text by default
                                text=self.theme.getThemeList()[0],
                                # available values
                                values=self.theme.getThemeList(),
                                # Size and position
                                size_hint=(.28, .05),pos_hint={'x':.05, 'y':.6})
        self.labelSpinnerMode = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["spinner2Text"]+"[/color]",
                        # Size and position
                        size_hint=(.28, .05),pos_hint={'x':.05, 'y':.5},
                        markup=True)
        self.spinnerMode = Spinner(  # Text by default
                                text=self.parent.parent.factory.modes[0],
                                # text=self.theme.getThemeList()[0],
                                # available values
                                values=self.parent.parent.factory.modes,
                                # values=self.theme.getThemeList(),
                                # Size and position
                                size_hint=(.28, .05),pos_hint={'x':.05, 'y':.45})
        # adds
        layout.add_widget(self.titleLabel)
        layout.add_widget(self.btnStart)
        layout.add_widget(self.labelSliderParticles)
        layout.add_widget(self.sliderParticles)
        layout.add_widget(self.labelNbParticles)
        layout.add_widget(self.labelSpinnerTheme)
        layout.add_widget(self.spinnerTheme)
        # layout.add_widget(self.labelSpinnerMode)
        # layout.add_widget(self.spinnerMode)
        # layout.add_widget(self.btnParams)
        self.spinnerTheme.bind(text=self.parent.updateTheme)
        self.spinnerMode.bind(text=self.parent.updateMode)
        self.sliderParticles.bind(value=self.updateNbParticles)
        self.btnStart.bind(on_press=self.parent.startGame)
        self.btnParams.bind(on_press=self.parent.initParams)
        

    def updateTitleColors(self, layout):
        layout.changeColor(self.theme.getParam("background_color"))
        self.titleLabel.text = "[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["titleText"]+"[/color]"
        self.labelSliderParticles.text = "[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["sliderText"]+"[/color]"
        self.labelNbParticles.text = "[color="+self.theme.getParam("main_color_hex")+"]"+str(self.sliderParticles.value)+"[/color]"
        self.labelSpinnerTheme.text = "[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["spinner1Text"]+"[/color]"
        self.sliderParticles.value_track_color = self.theme.getParam("main_color_dec")
        self.labelSpinnerMode.text = "[color="+self.theme.getParam("main_color_hex")+"]"+TitleView.texts["spinner2Text"]+"[/color]"

    
    def updateNbParticles(self, instance , value):
        self.labelNbParticles.text = "[color="+self.theme.getParam("main_color_hex")+"]"+str(self.sliderParticles.value)+"[/color]"

    def getParams(self):
        return [self.sliderParticles.value]
