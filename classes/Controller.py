

from classes.LightView import LightView
from classes.GameFactory import GameFactory
from classes.Field import Field
from classes.Particle import Particle
import time


class Controller:
    maxNbParticles = 500
    minNbParticles = 20
    def __init__(self):
        self.field = Field()
        self.view = LightView(self)
        self.factory = GameFactory(self)
        self.particleList = []
        self.vidProxy = None
        self.animations = []

        self.inGame = False

    def run(self):
        self.startView()
        pass

    def mainTick(self, data): #ITERATOR
        if self.inGame is True:
            frame , fString ,  color = self.vidProxy.getFrameAndString()

            self.view.updateVideo(frame , fString , color)
            self.view.tick()
            toAnimate = self.vidProxy.tick()
            if toAnimate != self.animations:
                self.animations = toAnimate
            self.field.tick()
            for each in reversed(self.particleList):
                if each.alive:
                    each.animate(self.animations)
                    each.tick()
                else:
                    self.particleList.remove(each)
                    self.particleList.append(Particle(self , self.view.theme))

    def startView(self):
        self.view.run()

    def startGame(self, nbParticles, field, modeIndex):
        self.factory.startGame(nbParticles, field, modeIndex, self.view.theme)