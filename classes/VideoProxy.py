
import numpy as np
import cv2

from classes.ParamsVidProxy import ParamsVidProxy
from classes.Utils import Utils
from classes.Hand import Hand
from classes.Action import Action
from classes.HandActionEval import HandActionEval

class VideoProxy():
    def __init__(self, parent):
        self.parent = parent
        self._vidCapture = cv2.VideoCapture(1)    

        #resize to window size
        self.resizePixel = self.parent.field.size[0]

        # # # # # # # # # # # # # # # # # # 
        # Get Parameters
        # # # # # # # # # # # # # # # # # # 
        params = ParamsVidProxy.getAllParams(2)

        self.hullColor = params["hull_color"]
        self.edgeColor = params["edge_color"]
        self.contourColor = params["contour_color"]
        self.nbEdges = params["nbEdges"]
        self.minThresh = params["minThresh"] 
        self.maxThresh = params["maxThresh"] 
        self.qualityLvl = params["qualityLvl"]
        self.corners = params["corners"]
        self.gausSize = params["gaussian_size"]
        self.centerSize = params["center_size"]
        self.centerColor = params["center_color"]
        self.minPixelArea = params["min_pixel_area"]
        self.frameDelay = params["frame_delay"]
        self.nbHandsSelect = params["number_hands_to_select"]
        self.actionDelay = params["number_framesD_wait_action"] * self.frameDelay
        self.animationDelay = params["number_framesD_wait_animation"] * self.frameDelay

        self.maxActionLength = params["max_nb_actions"]
        self.maxHandLength = params["max_nb_hands"]

        # # # # # # # # # # # # # # # # # # 
        # Shape tracking variables
        # # # # # # # # # # # # # # # # # # 
        self.maxArea =  {
                        "center":(0,0),
                        "x":0,
                        "y":0,
                        "index":0,
                        "area":0,
                        "contours":[],
                        "width":0,
                        "height":0,
                        "hull":0,
                        }
        self.hands = []
        self.actions = []
        self.oldHull = None
        self.newHull = None


        self.index = 0
        self.iCounter = 0
        self.doEvaluate = True
        self.actionsToAnimate = []
        

    # # # # # # # # # # # # # # # # # # 
    # OpenCV and Kivy methods
    # # # # # # # # # # # # # # # # # # 

    def releaseCapture(self):
        self._vidCapture.release()
        cv2.destroyAllWindows()

    def getFrameAndString(self):
        ret, frame = self._vidCapture.read()

        frame = self.getFlippedFrame(frame)

        frame = self.analyseImage(frame)

        # frame = self.getResizedImage(frame)
        frString = frame.tostring()
        return frame , frString , "bgra"

    # # # # # # # # # # # # # # # # # # 
    # Image transformation methods
    # # # # # # # # # # # # # # # # # # 

    def getFlippedFrame(self, frame):
         # flip vertically   (up, down)
        frame = cv2.flip(frame, 0)
         # flip horizontally <- ->
        frame = cv2.flip(frame, 1)
        return frame
    def getGrayscale(self, frame):
        return cv2.cvtColor(frame , cv2.COLOR_BGR2GRAY)

    def getThresh(self, frame, min , max):
        re2 , frame = cv2.threshold(frame, min, max , cv2.THRESH_BINARY)
        return frame

    def getGaus(self, frame):
        return cv2.GaussianBlur(frame,self.gausSize,0)

    def getRGBA(self, grayFrame):
        return cv2.cvtColor(grayFrame , cv2.COLOR_GRAY2BGRA)

    def getFrameWithAlpha(self, frame):
        b_channel, g_channel, r_channel , a_channel = cv2.split(frame)
        alpha_channel = np.ones(b_channel.shape, dtype=b_channel.dtype) * 50 #creating a dummy alpha channel image.
        frame = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))
        return frame

    def getResizedImage(self, frame):
        r = self.resizePixel / frame.shape[1]
        dim = (self.resizePixel, int(frame.shape[0] * r))
        return cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)

    def getTransformedFrame(self, frame):
        frame = self.getGrayscale(frame)
        frame = self.getGaus(frame)
        frame = self.getThresh(frame, self.minThresh, self.maxThresh)
        return frame

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # Drawing methods
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    def drawContours(self, frame, contours):
        cv2.drawContours(frame,contours,-1,self.contourColor,3)

    def drawCenter(self, frame):
        cv2.circle(frame, self.maxArea["center"], self.centerSize, self.centerColor , -1)

    def drawRectArea(self, x , y , w , h, frame):
        cv2.rectangle(frame, (x, y), (x + w, y + h), self.contourColor, 2)

    def drawHull(self, frame, hull, contours):
        for i in range(len(hull)):
            cv2.drawContours(frame, hull, i, self.hullColor, 1, 8)

    # # # # # # # # # # # # # # # # # # 
    # Shape tracking methods
    # # # # # # # # # # # # # # # # # # 
    
    def getContours(self, frame):
        contours , other = cv2.findContours(frame,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        return contours

    def getEdge(self, frame):
        return cv2.Canny(frame,self.minThresh,self.maxThresh)

    def getConvexHulls(self, contours, frame):
        if len(contours) == 0:
            return contours
        hull = []
        for i in range(len(contours)):
            hull.append(cv2.convexHull(contours[i], False))
        self.newHull = hull[0]
        if Utils.isEqual_NpArrays(self.newHull, self.oldHull) == False:
            self.oldHull = self.newHull
        return hull

    def getLargestHull(self, contours, frame):
        hull = self.getConvexHulls(contours, frame)
        for i , hl in reversed(list(enumerate(hull))):
            if i != self.maxArea["index"]:
                del hull[i]
        return hull

    def setMaxAreaParams(self, area,index,contour,x,y,h,w):
        self.maxArea["area"] = area
        self.maxArea["index"]=index
        self.maxArea["contours"]=contour
        self.maxArea["center"]=( int ( x + (w/2) ) , int( y+(h/2) ) )
        self.maxArea["height"] = int(h)
        self.maxArea["width"] = int(w)
        self.maxArea["x"] = int(x)
        self.maxArea["y"] = int(y)

    def getLargestArea(self, contours):
        x = y = w = h = 0
        for i , cnt in enumerate(contours):
            area = cv2.contourArea(cnt)
            if area <= self.minPixelArea:
                continue
            x,y,w,h = cv2.boundingRect(cnt)
            self.setMaxAreaParams(area,i,cnt,x,y,h,w)
        return x,y,w,h

    def getAreaRect(self, contours, frame):
        x , y , w , h = self.getLargestArea(contours)
        if 0 not in [x,y,w,h]:
            self.drawRectArea(x,y,w,h , frame)
        
    def setMaxArea(self, contours):
        for i , cnt in reversed(list(enumerate(contours))):
            area = cv2.contourArea(cnt)
            if(area>self.maxArea["area"]):
                self.maxArea["area"]=area
                self.maxArea["index"]=i
                self.maxArea["contours"]=cnt
            else:
                del cnt
                i = i-2
        return contours

    def getConvexPolygon(self, contours , frame):
        hull = self.getLargestHull(contours, frame)
        self.maxArea["hull"] = hull
        self.drawHull(frame , hull, contours)
        return frame

    # # # # # # # # # # # # # # # # # # 
    # Move tracking methods
    # # # # # # # # # # # # # # # # # # 

    def evaluateMove(self):
        if (self.index % self.frameDelay) == 0 and (self.doEvaluate == True):
            # print(" -- TICK -- ")
            newHand = Hand(self.maxArea["x"],
                                self.maxArea["y"],
                                self.maxArea["width"],
                                self.maxArea["height"],
                                self.maxArea["hull"],
                                self.maxArea["contours"],
                                self.maxArea["area"]
                                )
            if newHand.valid == True:
                self.hands.append(newHand)
            if len(self.hands) > 1 and len(self.hands) > self.nbHandsSelect:
                action = HandActionEval( self.hands[-self.nbHandsSelect:])
                if action.valid == True:
                    self.actions.append( action.actions )
                    self.hands = []
            pass
        self.index = self.index + 1

    def handListTooLarge(self):
        if len(self.hands) > self.maxHandLength:
            self.hands.pop(0)

    def actionListTooLarge(self):
        if self.iCounter > 0:
            if (self.iCounter+self.animationDelay) < self.index :
                self.actionsToAnimate = ["none"]
            if (self.iCounter+self.actionDelay) < self.index and self.doEvaluate == False:
                self.doEvaluate = True
                self.iCounter = 0
                self.actionsToAnimate = []
                self.actions = []
        else:
            if len(self.actions) >= self.maxActionLength:
                self.actionsToAnimate = Action.getActions(self.actions)
                self.doEvaluate = False
                self.iCounter = self.index
            else:
                self.actionsToAnimate = []
    
    # # # # # # # # # # # # # # # # # # 
    # Main video loop methods
    # # # # # # # # # # # # # # # # # # 
    def analyseImage(self,frame):
        frame = self.getTransformedFrame(frame)
        
        contours = self.getContours(frame)

        frame = self.getEdge(frame)
        frame = self.getRGBA(frame)

        
        countours = self.setMaxArea(contours)
        self.getAreaRect(contours,frame)
        frame = self.getConvexPolygon(contours, frame)

        if len(self.maxArea["contours"]) > 0:
            self.drawContours(frame,self.maxArea["contours"])
            self.drawCenter(frame)

        frame = self.getFrameWithAlpha(frame)
        return frame

    def tick(self):
        self.evaluateMove()
        self.actionListTooLarge()
        self.handListTooLarge()
        return self.actionsToAnimate