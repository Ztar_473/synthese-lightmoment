

class Relationship(): 
    possible_relationships = [
        {
            "name":"right",
            "number_of_hands":2,
            # "angle":{
            #     "min":67,
            #     "max":112,
            # }
            # "angle":{
            #     "min":45,
            #     "max":135,
            # }
            "angle":{
                "min":181,
                "max":360,
            }
        },
        {
            "name":"left",
            "number_of_hands":2,
            # "angle":{
            #     "min":292,
            #     "max":247,
            # }
            # "angle":{
            #     "min":225,
            #     "max":315,
            # }
            "angle":{
                "min":1,
                "max":180,
            }
        },
        {
            "name":"down",
            "number_of_hands":2,
            # "angle":{
            #     "min":157,
            #     "max":202,
            # }
            # "angle":{
            #     "min":136,
            #     "max":224,
            # }
            "angle":{
                "min":90,
                "max":270,
            }
        },
        {
            "name":"up", #01
            "number_of_hands":2,
            # "angle":{
            #     "min":0,
            #     "max":23,
            # }
            # "angle":{
            #     "min":0,
            #     "max":44,
            # }
            "angle":{
                "min":0,
                "max":89,
            }
        },
        {
            "name":"up", #02
            "number_of_hands":2,
            # "angle":{
            #     "min":337,
            #     "max":360,   
            # }
            # "angle":{
            #     "min":315,
            #     "max":360,   
            # }
            "angle":{
                "min":271,
                "max":360,   
            }
        },
        {
            "name":"larger",
            "number_of_hands":2,
            "translation":{
                "min":1,
                "max":1000000000
            }
        },
        {
            "name":"smaller",
            "number_of_hands":2,
            "translation":{
                "min":-1,
                "max":-1000000000
            }
        },
        {
            "name":"less_fingers",
            "number_of_hands":2,
        },
        {
            "name":"more_fingers",
            "number_of_hands":2,
        },
        {
            "name":"more_fingers",
            "number_of_hands":2,
        },
    ]