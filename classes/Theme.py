

class Theme:
    # static variable
    params = [
        {
            "name" : "Defaut",
            "background_color":[0, 0, 0, 1.0], #black color
            "particle_color":"",
            "main_color_hex":"FFEA00",
            "title_size":"47",
            "main_color_dec":[1, 0.918, 0, 1],
        },
        {
            "name" : "Noir",
            "background_color":[0, 0, 0, 1.0], #black color
            "particle_color":"",
            "main_color_hex":"FFF",
            "title_size":"45",
            "main_color_dec":[1, 1, 1, 1],
        },

        {
            "name" : "Blanc",
            "background_color":[1, 1, 1, 1], #white color
            "particle_color":"",
            "main_color_hex":"000",
            "title_size":"45",
            "main_color_dec":[0, 0, 0, 1],
        },

        {
            "name" : "Bleu",
            "background_color":[0.863, 0.878, 0.851, 1.0], #blue color
            "particle_color":"",
            "main_color_hex":"1C3144",
            "title_size":"45",
            "main_color_dec":[0.11, 0.192, 0.267, 1],
        },
    ]

    def __init__(self):
        self.selectedTheme = None
        pass
        

    def selectTheme(self, themeName):
        for each in Theme.params:
            if themeName in each.values():
                self.selectedTheme = each

    def getThemeList(self):
        theme_names = []
        for each in Theme.params:
            theme_names.append(each["name"])
        return theme_names
    
    def getParam(self, key):
        return self.selectedTheme[key.lower()]