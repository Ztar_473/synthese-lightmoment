
from classes.Relationship import Relationship
from classes.Utils import Utils

class HandRelationshipEval():
    negligeable_move = {
        "name":"none",
        "distance": {
            "min":0,
            "max":400,
        },
        "translation": {
            "min":0,
            "max":5000
        },
    }
    
    def __init__(self, fromHand = None , toHand= None  , distance= None  , angle= None , sizeTranslation= None , name= "" ):
        self.name = []
        if name != "":
            self.name = name
        self.fromHand = fromHand
        self.toHand = toHand
        self.properties = {
            "angle":angle,
            "distance": distance,
            "translation": sizeTranslation,
        }
        self.valid = self.isValid()
        if self.valid == True:
            self.evaluateRelationship()
    def evaluateRelationship(self):
        for rel in Relationship.possible_relationships:
            for i, (key, value) in enumerate(rel.items()):
                if key in self.properties:
                    if Utils.isBetweenAC(value["min"], self.properties[key], value["max"]) == True:
                        self.name.append(rel["name"])
    
    def isValid(self):
        for i, (key, value) in enumerate(HandRelationshipEval.negligeable_move.items()):
            if key in self.properties:
                if Utils.isBetweenAC(value["min"], self.properties[key], value["max"]) == False:
                    return True
        self.name = HandRelationshipEval.negligeable_move["name"]
        return False