import random
from kivy.graphics import Color, Ellipse
from kivy.uix.widget import Widget
import time

from classes.Effects import Effects

class Particle(Widget):
    ParticleCount = 0
    maxSize = 30
    minSize = 2
    maxVelocity = 10
    minVelocity = 1
    tolerance = 15

    def __init__(self, parent, theme):
        self.parent = parent
        self.index = Particle.ParticleCount
        Particle.ParticleCount = Particle.ParticleCount + 1
        self.diameter = random.randint(Particle.minSize, Particle.maxSize)
        
        self.xPosition = random.randint(1 + Particle.tolerance, parent.field.size[0] - Particle.tolerance)
        self.yPosition = random.randint(1 + Particle.tolerance, parent.field.size[1] - Particle.tolerance)
        self.xVelocity = random.randint(Particle.minVelocity, Particle.maxVelocity)
        self.yVelocity = random.randint(Particle.minVelocity, Particle.maxVelocity)
        self.accerelation = 0.5
        self.alive = True
        self.theme = theme
        pass

        self.checkPoint = {
            "xV" : self.xVelocity,
            "yV" : self.yVelocity,
            "xP" : self.xPosition,
            "yP" : self.yPosition,
            "acceleration" : [],
        }
    def advance(self):
        self.xPosition = self.xPosition + self.xVelocity
        self.yPosition = self.yPosition + self.yVelocity

    def bounce(self, direction):
        if direction == "y":
            self.yVelocity = -self.xVelocity
            self.xVelocity = -self.yVelocity
        else:
            self.yVelocity = -self.yVelocity
            self.xVelocity = -self.xVelocity
        pass

    def mirrorVelocity(self):
        pass

    def isDead(self):
        sizeX = self.parent.field.size[0]
        sizeY = self.parent.field.size[1]
        if self.xPosition + self.diameter/2 > sizeX or self.xPosition - self.diameter/2 < 0:
            self.bounce( "x" )
        elif self.yPosition + self.diameter/2 > sizeY or self.yPosition - self.diameter/2 < 0:
            self.bounce("y")

        if self.xPosition > sizeX + self.diameter * Particle.tolerance or self.xPosition < 0 - self.diameter * Particle.tolerance:
            self.alive = False
            # return False
        elif self.yPosition > sizeY + self.diameter * Particle.tolerance or self.yPosition < 0 - self.diameter * Particle.tolerance:
            self.alive = False

    def addVelocity(self, xAcceleration,yAcceleration):
        self.xVelocity = self.xVelocity + xAcceleration
        self.yVelocity = self.yVelocity + yAcceleration
        pass
    def substractVelocity(self, xAcceleration,yAcceleration):
        self.xVelocity = self.xVelocity - xAcceleration
        self.yVelocity = self.yVelocity - yAcceleration

    def draw(self):
        with self.parent.field.canvas:
            Color(  self.theme.getParam("main_color_dec")[0],
                    self.theme.getParam("main_color_dec")[1],
                    self.theme.getParam("main_color_dec")[2]    )
            Ellipse(pos=(self.xPosition - self.diameter / 2, self.yPosition - self.diameter / 2), size=(self.diameter, self.diameter))
        pass

    def getRandAcceleration(self):
        return random.randint(Particle.maxVelocity, Particle.maxVelocity)

    def velocityCheckPoint(self):
        self.checkPoint["xV"] = self.xVelocity/2
        self.checkPoint["yV"] = self.yVelocity/2
        pass

    def restoreVelocity(self):
        self.xVelocity = self.checkPoint["xV"]
        self.yVelocity = self.checkPoint["yV"]
        pass

    def equalsCheckPoint(self):
        if self.xVelocity != self.checkPoint["xV"] or self.yVelocity != self.checkPoint["yV"]:
            return True
        return False

    def backToOrigin(self):
        if(self.xPosition <= self.checkPoint["xP"]):
            if(self.xVelocity < 15):
                self.xVelocity = self.xVelocity + self.accerelation
        else:
            if(self.xVelocity > -15):
                self.xVelocity = self.xVelocity - self.accerelation

        if(self.yPosition <= self.checkPoint["yP"]):
            if(self.yVelocity < 15):
                self.yVelocity = self.yVelocity + self.accerelation
        else:
            if(self.yVelocity > -15):
                self.yVelocity = self.yVelocity - self.accerelation

    def animate(self, animationList):
        if len(animationList) > 0:
            self.velocityCheckPoint()
            acceleration = []
            for each in Effects.possible_effects:
                if each["actions"] == animationList:
                    acceleration.append(each["acceleration"])

            if len(acceleration) > 0:
                for each in acceleration:
                    self.addVelocity(each[0], each[1])
                self.checkPoint["acceleration"] = acceleration
            else:
                if "none" in animationList:
                    for each in self.checkPoint["acceleration"]:
                        self.substractVelocity(each[0], each[1])

    def tick(self):
        self.isDead()
        self.advance()
        self.draw()
        pass
