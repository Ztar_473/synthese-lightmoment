
class ParamsVidProxy(): 
    p_static = {
        "hull_color":(255, 0, 0), #blue
        "edge_color":(255, 0, 0), #blue
        "contour_color":(0, 255, 0), #green
        "center_color":(0, 0, 255), #red
        "number_hands_to_select":6,
        "number_framesD_wait_action":6,
        "number_framesD_wait_animation":10,
        "max_nb_actions":4,
        "max_nb_hands":10,
        "center_size":5,
        # "animation_delay":2, #10
    }
    params = [
        {
            "name" : "testing",
            "nbEdges":40,
            "minThresh":60,
            "maxThresh":115,
            "qualityLvl":0.01,
            "corners":1,
            "gaussian_size":(61,61), #must be odd number and positive
            "min_pixel_area":50, #50
            "frame_delay":6, #10
        },
        {
            "name" : "night",
            "nbEdges":40,
            "minThresh":115,
            "maxThresh":300,
            "qualityLvl":0.01,
            "corners":1,
            "gaussian_size":(21,21), #must be odd number and positive
            "min_pixel_area":100, #50
            "frame_delay":10, #
        },
        {
            "name" : "darker",
            "nbEdges":40,
            "minThresh":115,
            "maxThresh":250,
            "qualityLvl":0.05,
            "corners":1,
            "gaussian_size":(21,21), #must be odd number and positive
            "min_pixel_area":100, #50
            "frame_delay":2, #
        },
        # {
        #     "name" : "view",
        #     "nbEdges":40,
        #     "minThresh":60,
        #     "maxThresh":115,
        #     "qualityLvl":0.01,
        #     "corners":1,
        # },
        # {
        #     "name" : "house",
        #     "nbEdges":40,
        #     "minThresh":60,
        #     "maxThresh":115,
        #     "qualityLvl":0.01,
        #     "corners":1,
        #     "resize_pixel":1200,
        #     "hull_color":(255, 0, 0), #blue
        #     "edge_color":(255, 0, 0), #blue
        #     "contour_color":(0, 255, 0), #green
        #     "gaussian_size":(9,9)
        # },
        # {
        #     "name" : "school",
        #     "nbEdges":40,
        #     "minThresh":60,
        #     "maxThresh":115,
        #     "qualityLvl":0.01,
        #     "corners":1,
        # },

    ]
    @staticmethod
    def getAllParams(place):
        params = {}
        if type(place) == str:
            for index, param in  enumerate(ParamsVidProxy.params):
                if place in param["name"]:
                    params = ParamsVidProxy.params[index]
        else:
            params = ParamsVidProxy.params[place]
        params.update(ParamsVidProxy.p_static)
        return params