# Kivy graphics
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.anchorlayout import AnchorLayout
# Kivy Interval
from kivy.clock import Clock
# Kivy widgets
from kivy.graphics.texture import Texture

# Light Moment Libraries
from classes.VideoProxy import VideoProxy
from classes.Theme import Theme
from classes.Field import Field
from classes.TitleView import TitleView
from classes.ParamsView import ParamsView

import cv2


class LightView(App):
    def __init__(self, parent):
        super(LightView, self).__init__()
        self.parent = parent
        # Window parameters
        self.title = "Light Moment"
        self.icon = 'images/light.png'
        # The Mode
        self.selectedMode = 0
        # The Theme
        self.theme = Theme()
            # select first theme by default
        self.theme.selectTheme(self.theme.getThemeList()[0])
        # background
        self.backgroundLayout = AnchorLayout()
        self.layout = self.parent.field
        # Templates
        self.view = None
        # Video
        self.videoImage = None

        

    def build(self):
        self.layout = self.initTitle(1)
        self.backgroundLayout.add_widget(self.layout)
        Clock.schedule_interval(self.parent.mainTick, 1.0/33.0)
        return self.backgroundLayout


    def initTitle(self, instance):
        self.cleanCanvas()
        self.view = TitleView(self, self.layout, self.theme)
        self.view.setTitleWidgets(self.layout)
        return self.layout
    
    def startGame(self, instance):
        nbParticles = self.view.sliderParticles.value
        field = self.layout
        mode = self.selectedMode
        self.parent.startGame(nbParticles, field, mode)

    def cleanCanvas(self):
        self.parent.field.cleanCanvas()
        
    
    def initParams(self, instance):
        self.cleanCanvas()
        self.view = ParamsView(self, self.layout, self.theme)
        self.view.setTitleWidgets(self.layout)
        return self.layout

    def updateMode(self,spinner,text):
        self.selectedMode = self.parent.factory.modes.index(text)

    def updateTheme(self,spinner,text):
        self.theme.selectTheme(text)
        self.view.updateTitleColors(self.layout)

    def updateVideo(self, frame, frString , color):
        texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt=color)
        texture.blit_buffer(frString, colorfmt=color, bufferfmt='ubyte')
        self.videoImage.texture = texture

    
    def on_stop(self):
        #without this, app will not exit even if the window is closed
        self.parent.vidProxy._vidCapture.release()
        # self.capture.release()

    def tick(self):
        pass
     