
from kivy.uix.floatlayout import FloatLayout
from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.graphics import Rectangle
from kivy.graphics import Color

Builder.load_string('''
<Field>:
    #default
    background : 0, 0, 0, 1.0
    canvas:
        Color:
            rgba: self.background
        Rectangle:
            pos: self.pos
            size: self.size
''')

# class RootWidget(Widget):
#     pass
class Field(FloatLayout):
    background = ListProperty()

    def __init__(self, *args):
        super(Field, self).__init__(*args)
        self.color = [0, 0, 0, 1.0]

    def changeColor(self, color):
        self.color = color
        self.background = color
    
    def addBackground(self):
        with self.canvas:
            Color(self.color[0], self.color[1], self.color[2])
            Rectangle(pos=(self.pos),size=(self.size))
        # self.canvas.
        pass

    def tick(self):
        self.cleanCanvas()
        self.addBackground()

    def cleanCanvas(self):
        self.canvas.clear()
        self.addBackground()
        
    def voidField(self):
        self.clear_widgets()
        self.canvas.clear()
    