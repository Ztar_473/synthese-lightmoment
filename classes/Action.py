
from classes.Utils import Utils

class Action():
    possible_actions = [
        {
            "name":"none",
            "min_nb_relationships":1,
            "action":"none",
        },
        {
            "name":"right_gravity",
            "min_nb_relationships":1,
            "action":"right",
        },
        {
            "name":"left_gravity",
            "min_nb_relationships":1,
            "action":"left",
        },
        {
            "name":"up_gravity",
            "min_nb_relationships":1,
            "action":"up",
        },
        {
            "name":"down_gravity",
            "min_nb_relationships":1,
            "action":"down",
        },
        {
            "name":"jump",
            "min_nb_relationships":2,
            "action":"larger",
        },
    ]


    @staticmethod
    def getActions(listActions):
        flatActions = Utils.get1DList(listActions)
        actionsStats = {}
        sortedStats = None
        for each in Action.possible_actions:
            key = each["action"]
            concurrence = flatActions.count(key)
            if concurrence > 0 and key:
                actionsStats[key] = concurrence
        sortedStats = sorted(actionsStats.items(), key=lambda kv: kv[1])
        actions = []
        if len(sortedStats) > 1:
            if (sortedStats[-1][1] == sortedStats[-2][1]):
                actions.append(sortedStats[-2][0])
        actions.append(sortedStats[-1][0])
        return actions
        pass