# widgets
from kivy.uix.label import Label
from kivy.uix.button import  Button
from kivy.uix.spinner import Spinner
from kivy.uix.slider import Slider

class ParamsView:
    texts = {
            "titleText" : "Parametres avancées",
            "sliderText" : "Minimum thresh",
            "sliderText1" : "Maximum thresh",
            "sliderText2" : "Quality level",
            "sliderText3" : "Pixel resize",
            "sliderText4" : "Gaussian blur",
        }
    def __init__(self, parent, layout, theme):
        self.parent = parent
        self.layout = layout
        self.theme = theme
        # Title widgets
        self.titleLabel = None
        self.btnStart = None
        self.labelSliderParticles = None
        self.sliderParticles = None
        self.labelNbParticles = None
        self.labelSpinnerTheme = None
        self.spinnerTheme = None
        self.labelSpinnerMode = None
        self.spinnerMode = None
        pass
    
    def setTitleWidgets(self, layout):
        self.titleLabel = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+ParamsView.texts["titleText"]+"[/color]",
                        # Size and position
                        size_hint=(.5, .20),pos_hint={'x':0.1, 'y':.8},
                        # Parameters by theme
                        font_size=self.theme.getParam("title_size"), markup=True)
        self.btnStart = Button(   # Text in the button
                        text='Retourner',
                        # Size and position
                        size_hint=(.11, .06),pos_hint={'x':.05, 'y':.05})
        

        self.labelSliderParticles = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+ParamsView.texts["sliderText"]+"[/color]",
                        # Size and position
                        size_hint=(.28, .05),pos_hint={'x':.05, 'y':.75},
                        markup=True)
        self.sliderParticles = Slider(value_track=True,
                        max=self.parent.parent.maxNbParticles, min=self.parent.parent.minNbParticles, step=1,
                        # Size and position
                        size_hint=(.27, .05),pos_hint={'x':.04, 'y':.72},
                        cursor_size=(16,16),
                        # Parameters by theme
                        value_track_color=self.theme.getParam("main_color_dec"))
        self.labelNbParticles = Label(  # Title with theme colors
                        text="[color="+self.theme.getParam("main_color_hex")+"]"+str(self.sliderParticles.value)+"[/color]",
                        # Size and position
                        size_hint=(0, .05),pos_hint={'x':.32, 'y':.72},
                        markup=True)

        layout.add_widget(self.titleLabel)
        layout.add_widget(self.btnStart)
        layout.add_widget(self.labelSliderParticles)
        layout.add_widget(self.sliderParticles)
        layout.add_widget(self.labelNbParticles)
        self.sliderParticles.bind(value=self.updateNbParticles)
        self.btnStart.bind(on_press=self.parent.initTitle)
        

    def updateTitleColors(self, layout):
        layout.changeColor(self.theme.getParam("background_color"))
        self.titleLabel.text = "[color="+self.theme.getParam("main_color_hex")+"]"+ParamsView.texts["titleText"]+"[/color]"
        self.labelSliderParticles.text = "[color="+self.theme.getParam("main_color_hex")+"]"+ParamsView.texts["sliderText"]+"[/color]"
        self.labelNbParticles.text = "[color="+self.theme.getParam("main_color_hex")+"]"+str(self.sliderParticles.value)+"[/color]"
        self.labelSpinnerTheme.text = "[color="+self.theme.getParam("main_color_hex")+"]"+ParamsView.texts["spinner1Text"]+"[/color]"
        self.sliderParticles.value_track_color = self.theme.getParam("main_color_dec")
        self.labelSpinnerMode.text = "[color="+self.theme.getParam("main_color_hex")+"]"+ParamsView.texts["spinner2Text"]+"[/color]"

    
    def updateNbParticles(self, instance , value):
        self.labelNbParticles.text = "[color="+self.theme.getParam("main_color_hex")+"]"+str(self.sliderParticles.value)+"[/color]"

    def getParams(self):
        return [self.sliderParticles.value]
