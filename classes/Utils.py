
import numpy as np
import math
from itertools import chain

class Utils():
    
    @staticmethod
    def getDist(a, b):
        return math.sqrt((a[0] - b[0])**2 + (b[1] - a[1])**2)

    @staticmethod
    def getAngle(a,b,c):
        bc = (c[1]-b[1], c[0]-b[0])
        ba = (a[1]-b[1], a[0]-b[0])

        ang = math.degrees(math.atan2(ba[0],ba[1]) - math.atan2(bc[0], bc[1]) )
        return ang + 360 if ang < 0 else ang
   
    @staticmethod
    def getAngle2(a,b,c):
        bc = (c[1]-b[1], c[0]-b[0])
        ba = (a[1]-b[1], a[0]-b[0])

        ang = math.degrees(math.atan2(ba[0],ba[1]) - math.atan2(bc[0], bc[1]) )

        return ang
   
    @staticmethod
    def isEqual_NpArrays(listA, listB):
        if np.array_equal(listA, listA):
            return True
        return False

    
    @staticmethod
    def getSizeTranslation(areaA, areaB):
        return areaA - areaB
        pass

    @staticmethod
    def isBetweenAC(a, b, c):
        if a <= b and b <= c:
            return True
        return False
    
    @staticmethod
    def getCoocurrenceList(list):
        pass
    @staticmethod
    def get1DList(l):
        return sum(l, [])